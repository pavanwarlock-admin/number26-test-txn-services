package com.number26.pv.api.output;

public class TransactionSumOutput {

	private Double sum;
	
	public TransactionSumOutput(Double sum) {
		super();
		this.sum = sum;
	}

	public Double getSum() {
		return sum;
	}

	public void setSum(Double sum) {
		this.sum = sum;
	}
	
}
