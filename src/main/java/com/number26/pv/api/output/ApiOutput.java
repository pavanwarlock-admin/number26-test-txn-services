package com.number26.pv.api.output;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL )
public class ApiOutput {

	private ApiStatus status = ApiStatus.OK;
	//TODO use errorCode to map different internal errorCodes and meaningful error messsages 
	//visible to user
	
	private String errorMessage;
	
	public ApiOutput(){
		
	}
	public ApiOutput(ApiStatus status) {
		super();
		this.status = status;
	}
	
	public ApiOutput(ApiStatus status, String errorMessage) {
		super();
		this.status = status;
		this.errorMessage = errorMessage;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public ApiStatus getStatus() {
		return status;
	}

	public void setStatus(ApiStatus status) {
		this.status = status;
	}
	
	@JsonIgnore
	public boolean isSuccess(){
		return this.status == ApiStatus.OK;
	}
}
