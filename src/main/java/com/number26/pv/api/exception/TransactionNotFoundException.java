package com.number26.pv.api.exception;

public class TransactionNotFoundException extends Exception{

	private static final long serialVersionUID = 1L;
	
	private Long txnId;

	public TransactionNotFoundException(Long txnId) {
		super();
		this.txnId = txnId;
	}

	public Long getTxnId() {
		return txnId;
	}

	public void setTxnId(Long txnId) {
		this.txnId = txnId;
	}
	
	public String getMessage(){
		return new StringBuilder().append("Transaction ").append(txnId).append(" does not exist").toString();
	}
}
