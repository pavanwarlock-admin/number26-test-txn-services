package com.number26.pv.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.number26.pv.api.exception.TransactionNotFoundException;
import com.number26.pv.api.input.TransactionInput;
import com.number26.pv.api.model.Transaction;
import com.number26.pv.api.output.ApiOutput;
import com.number26.pv.api.output.ApiStatus;
import com.number26.pv.api.output.TransactionOutput;
import com.number26.pv.api.output.TransactionSumOutput;
import com.number26.pv.api.service.TransactionService;

@RestController
@RequestMapping("transactionservice")
public class TransactionController {
	

	@Autowired private TransactionService transactionService;
	
	@ApiOperation(value = "Create transaction")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "transactionId", value = "Id of the transaction", required = true, dataType = "long", paramType = "path")
      })
	
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Created", response = ApiOutput.class),
			@ApiResponse(code = 420, message = "UnProcessable Entity", response = ApiOutput.class),
			@ApiResponse(code = 400, message = "Bad request")
	})
	@RequestMapping(value = "/transaction/{transactionId}", 
			method = RequestMethod.PUT,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiOutput> createTransaction(@PathVariable(value="transactionId") Long transactionId, 
			@RequestBody @ApiParam TransactionInput txnInput){
		;
		
		ApiOutput apiOut = transactionService.validate(txnInput);
		if(!apiOut.isSuccess()) return new ResponseEntity<ApiOutput>(apiOut, HttpStatus.UNPROCESSABLE_ENTITY);
		
		Transaction txn = transactionService.create(txnInput, transactionId);
		
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setLocation(linkTo(methodOn(TransactionController.class).createTransaction(txn.getTransactionId(), null)).toUri());
		return new ResponseEntity<ApiOutput>(new ApiOutput(), httpHeaders, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Get transaction")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "transactionId", value = "Id of the transaction", required = true, dataType = "long", paramType = "path")
      })
	@RequestMapping(value = "/transaction/{transactionId}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TransactionOutput> getTransaction(@PathVariable(value="transactionId") Long transactionId) throws TransactionNotFoundException{ 
		return new ResponseEntity<TransactionOutput>(transactionService.findById(transactionId), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Get all transactions by type")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "type", value = "Type of the transaction", required = true, dataType = "String", paramType = "path")
      })
	@RequestMapping(value = "/types/{type}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Long>> getTransactionIds(@PathVariable(value="type") String type){
		return new ResponseEntity<List<Long>>(transactionService.findAllByType(type), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Sum of transtive transaction amounts")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "transactionId", value = "Id of the transaction", required = true, dataType = "long", paramType = "path")
      })
	@RequestMapping(value = "/sum/{transactionId}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TransactionSumOutput> getSum(@PathVariable(value="transactionId") Long transactionId) throws TransactionNotFoundException{
		return new ResponseEntity<TransactionSumOutput>(transactionService.getSum(transactionId), HttpStatus.OK);
	}
	
	
	@ExceptionHandler(TransactionNotFoundException.class)
	public ResponseEntity<ApiOutput> handleNotFound(Exception e){
		return new ResponseEntity<ApiOutput>(new ApiOutput(ApiStatus.ERROR, e.getMessage()), HttpStatus.NOT_FOUND);
	}
}
