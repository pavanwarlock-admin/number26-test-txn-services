package com.number26.pv.api.model;

import java.util.Optional;

/**
 * @author pv 
 */

public class Transaction {

	private Long transactionId;	
	private Double amount;
	private String type;
	private Optional<Long> parentId = Optional.empty();
	
	public Transaction(){
		
	}
	
	public Transaction(Long transactionId, Double amount, String type) {
		super();
		this.transactionId = transactionId;
		this.amount = amount;
		this.type = type;
		this.parentId = Optional.empty();
	}
	
	public Transaction(Long transactionId, Double amount, String type, Long parentId){
		this(transactionId, amount, type);
		this.parentId = Optional.ofNullable(parentId);
	}
	
	public Long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Optional<Long> getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = Optional.ofNullable(parentId);
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
