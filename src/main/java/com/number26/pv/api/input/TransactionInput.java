package com.number26.pv.api.input;

import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonProperty;

import flexjson.JSON;


public class TransactionInput {
	
	private String type;
	private Double amount;
	private Optional<Long> parentId = Optional.empty();
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Double getAmount() {
		return amount;
	}
	
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@JSON(name="parent_id")
	public Optional<Long> getParentId() {
		return parentId;
	}

	@JsonProperty(value="parent_id")
	public void setParentId(Long parentId) {
		this.parentId = Optional.ofNullable(parentId);
	}
}
