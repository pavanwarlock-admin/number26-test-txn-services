package com.number26.pv.api.service.repo;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.number26.pv.api.model.Transaction;

@Service("transactionRepository")
public class TransactionRepository {
	
	//TODO interface this class

	public static Map<Long, Transaction> txnMap = new HashMap<Long, Transaction>();

	public void save(Transaction txn){
		txnMap.put(txn.getTransactionId(), txn);
	}
	
	public Transaction get(Long id){
		return txnMap.get(id);
	}
	
	public Collection<Transaction> getAll(){
		return txnMap.values();
	}
}
