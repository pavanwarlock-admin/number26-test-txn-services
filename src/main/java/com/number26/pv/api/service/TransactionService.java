package com.number26.pv.api.service;

import java.util.List;

import com.number26.pv.api.exception.TransactionNotFoundException;
import com.number26.pv.api.input.TransactionInput;
import com.number26.pv.api.model.Transaction;
import com.number26.pv.api.output.ApiOutput;
import com.number26.pv.api.output.TransactionOutput;
import com.number26.pv.api.output.TransactionSumOutput;

public interface TransactionService {
	public Transaction create(TransactionInput txnInput, Long transactionId);
	public ApiOutput validate(TransactionInput txnInput);
	public TransactionOutput findById(Long transactionId) throws TransactionNotFoundException;
	public List<Long> findAllByType(String type);
	public TransactionSumOutput getSum(Long transactionId) throws TransactionNotFoundException;
	
}
