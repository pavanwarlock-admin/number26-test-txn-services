package com.number26.pv.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.number26.pv.api.exception.TransactionNotFoundException;
import com.number26.pv.api.input.TransactionInput;
import com.number26.pv.api.model.Transaction;
import com.number26.pv.api.output.ApiOutput;
import com.number26.pv.api.output.ApiStatus;
import com.number26.pv.api.output.TransactionOutput;
import com.number26.pv.api.output.TransactionSumOutput;
import com.number26.pv.api.service.TransactionService;
import com.number26.pv.api.service.repo.TransactionRepository;
import com.number26.pv.api.util.JsonUtil;


@Service("transactionService")
public class TransactionServiceImpl implements TransactionService{
	
	@Autowired TransactionRepository transactionRepository;
	

	private static final Logger logger = LoggerFactory.getLogger(TransactionServiceImpl.class);
	
	@Override
	public Transaction create(TransactionInput txnInput, Long transactionId) {
		
		logger.debug("Creating transaction for request: Id - "+ transactionId+" "+  JsonUtil.toJson(txnInput));
		
		Transaction txn = new Transaction();
		txn.setTransactionId(transactionId);
		txn.setAmount(txnInput.getAmount());
		txn.setType(txnInput.getType());
		txn.setParentId(txnInput.getParentId().orElse(null));
		transactionRepository.save(txn);
		
		logger.debug("Txn created successfully - Id: "+transactionId);
		
		return txn;
	}

	//Can also use JSR-349 Bean Validation API; 
	//This method of validation is preferred for more control on error codes , err
	@Override
	public ApiOutput validate(TransactionInput txnInput) {
		if(txnInput.getAmount() == null || !StringUtils.hasText(txnInput.getType())){
			logger.error("Required field is null for request: "+JsonUtil.toJson(txnInput));
			
			return new ApiOutput(ApiStatus.ERROR, "One of the required field is null");
		}
		if(txnInput.getParentId().isPresent()){
			if(!getTxn(txnInput.getParentId().get()).isPresent()){
				logger.error("Failed creating transaction. Parent transasction {"+ txnInput.getParentId().get() +"} not found");
				return new ApiOutput(ApiStatus.ERROR,"Parent txn not found");
			}
		}
		return new ApiOutput();
	}

	@Override
	public TransactionOutput findById(Long transactionId) throws TransactionNotFoundException{
		Optional<Transaction> txn = getTxn(transactionId);
		if(txn.isPresent()){
			TransactionOutput out = new TransactionOutput();
			Transaction t = txn.get();
			out.setAmount(t.getAmount());
			out.setParentId(t.getParentId().orElse(null));
			out.setType(t.getType());
			return out;
		}else{
			logger.error("Transaction {"+transactionId+"} not found");
			throw new TransactionNotFoundException(transactionId);
		}
	}

	@Override
	public List<Long> findAllByType(String type) {
		logger.debug("Fetching all txns of type: "+type);		
		//use parallel stream if necessary
		List<Long> txnIds =  transactionRepository.getAll().stream()
						 .filter(e -> e.getType().equals(type))
						 .map(e -> e.getTransactionId())
						 .collect(Collectors.toList());
		
		logger.debug("Found "+txnIds.size()+" of type: "+type);
		
		return txnIds;
	}

	/**
	 * Computes sum of all transitively dependant transactions of given transactionId
	 */
	@Override
	public TransactionSumOutput getSum(Long transactionId) throws TransactionNotFoundException {
		logger.debug("Computing sum for txn "+transactionId);
		return new TransactionSumOutput(getDependantTxns(transactionId).stream()
									   .mapToDouble(e -> e.getAmount())
									   .sum());
	}
	
	private Optional<Transaction> getTxn(Long transactionId){
		return Optional.ofNullable(transactionRepository.get(transactionId));
	}
	
	private List<Transaction> getDependantTxns(Long transactionId) throws TransactionNotFoundException{
		logger.debug("Fetching dependant txns for "+transactionId);
		
		Optional<Transaction> txn = getTxn(transactionId);
		if(txn.isPresent()){
			List<Transaction> txns = new ArrayList<Transaction>();
			txns.add(txn.get());
			while(txn.get().getParentId().isPresent()){
				txn = Optional.ofNullable(transactionRepository.get(txn.get().getParentId().get()));
				txns.add(txn.get());
			}
			logger.debug("Found "+txns.size()+" transitively dependant txns");
			return txns;
		}else{
			logger.error("Transaction {"+transactionId+"} not found");
			throw new TransactionNotFoundException(transactionId);
		}
	}
}
