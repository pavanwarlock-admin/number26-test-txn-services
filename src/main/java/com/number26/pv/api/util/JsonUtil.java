package com.number26.pv.api.util;

import flexjson.JSONSerializer;

public class JsonUtil {

	public static String toJson(Object obj){
		return new JSONSerializer().exclude("*.class").deepSerialize(obj);
	}
}
