package com.number26.pv.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import com.number26.pv.api.model.Transaction;
import com.number26.pv.api.service.repo.TransactionRepository;
import com.number26.pv.api.util.JsonUtil;
import com.number26.pv.main.Application;

import flexjson.JSONSerializer;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebIntegrationTest("server.port:9000")
public class TransactionControllerTest {
	
	private static String CONTENT = MediaType.APPLICATION_JSON_VALUE;

	@Autowired private TransactionRepository transactionRepository;
	@Autowired private WebApplicationContext webApplicationContext;
	
	private MockMvc mockMvc;
	
	@Before
	public void before(){
		this.mockMvc = webAppContextSetup(webApplicationContext).build();
		
		transactionRepository.save(new Transaction(1L, 100d, "shoes"));
		transactionRepository.save(new Transaction(2L, 200d, "shoes"));
		transactionRepository.save(new Transaction(3L, 150d, "food"));
		transactionRepository.save(new Transaction(4L, 150d, "grocery", 3L));
	}
	
	
	@Test
	public void testGetTransaction() throws Exception{
		mockMvc.perform(get("/transactionservice/transaction/1"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
			.andExpect(jsonPath("$.amount").value(100d))
			.andExpect(jsonPath("$.type").value("shoes"))
			.andExpect(jsonPath("$.parent_id").doesNotExist());
		
		mockMvc.perform(get("/transactionservice/transaction/4"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
			.andExpect(jsonPath("$.amount").value(150d))
			.andExpect(jsonPath("$.type").value("grocery"))
			.andExpect(jsonPath("$.parent_id").value(3));
	}
	
	@Test
	public void testInvalidTransaction() throws Exception{
		mockMvc.perform(get("/transactionservice/transaction/10"))
			.andExpect(status().isNotFound())
			.andExpect(jsonPath("$.status").value("ERROR"));
	}
	
	@Test
	public void testTxnCreate() throws Exception{
		TxnInput input = new TxnInput(100d, "mobile", 2L);
		
		String json = toJson(input);
		
		mockMvc.perform(put("/transactionservice/transaction/5")
			.contentType(CONTENT)
			.content(json))
			.andExpect(status().isCreated())
			.andExpect(jsonPath("$.status").value("OK"));
			
	}
	
	@Test
	public void testTxnCreateInvalidInput() throws Exception{
		TxnInput input = new TxnInput(100d, null, 2L);
		
		String json = toJson(input);
		
		mockMvc.perform(put("/transactionservice/transaction/5")
			.contentType(CONTENT)
			.content(json))
			.andExpect(status().isUnprocessableEntity())
			.andExpect(jsonPath("$.status").value("ERROR"));
			
	}
	
	@Test
	public void testTxnTypes() throws Exception{
		MvcResult result = mockMvc.perform(get("/transactionservice/types/shoes"))
				.andExpect(status().isOk()).andReturn();
		JSONParser parser = new JSONParser();
		JSONArray jsonArray = (JSONArray)parser.parse(result.getResponse().getContentAsString());
		
		Assert.assertEquals("Txn array size mismatch",2, jsonArray.size());
		
		Long[] expected = new Long[]{1L, 2L};
		
		Assert.assertArrayEquals("Array mismatch", expected, jsonArray.toArray());
		
		result = mockMvc.perform(get("/transactionservice/types/grocery")).andReturn();
		jsonArray = (JSONArray)parser.parse(result.getResponse().getContentAsString());
		Assert.assertEquals("Txn array size mismatch",1, jsonArray.size());
	}
	
	@Test
	public void testTxnSum() throws Exception{
		 mockMvc.perform(get("/transactionservice/sum/4"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.sum").value(300d));
		 
		 TxnInput input = new TxnInput(100.1d, "mobile", 4L);
			
			String json = toJson(input);
			
		 mockMvc.perform(put("/transactionservice/transaction/5")
			.contentType(CONTENT)
			.content(json))
			.andExpect(status().isCreated())
			.andExpect(jsonPath("$.status").value("OK"));
			
		 mockMvc.perform(get("/transactionservice/sum/5"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.sum").value(400.1d));
		 
		 mockMvc.perform(get("/transactionservice/sum/1"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.sum").value(100d));
	}
	
	
	
	private final String toJson(Object obj){
		return JsonUtil.toJson(obj);
	}
	
}
