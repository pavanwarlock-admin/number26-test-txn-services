package com.number26.pv.test;

import flexjson.JSON;

public class TxnInput {

	private String type;
	private Double amount;
	private Long parentId;
	
	
	public TxnInput(Double amount, String type,
			Long parentId) {
		super();
		this.type = type;
		this.amount = amount;
		this.parentId = parentId;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	@JSON(name="parent_id")
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	
	
}
