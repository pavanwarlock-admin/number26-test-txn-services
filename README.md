Number26-txn-services
=====================

RESTful APIs for transactions for number26

Server exposing APIs for managing transactions from test specs. All transactions are stored in memory of the application. 

Technologies used:
-----------------
Spring boot 1.3.3  
Maven -  build  
Integration & Unit tests using Junit4 & Spring mock mvc

Swagger for documentation. API docs can be found at http://localhost:8080/swagger-ui.html

Directory Structure
-------------------

src -  main and test source code  
config - configs for logging & application

Config
------
logback.xml - Location of logs ( default dist/logs)  
application.yml  - Application config, profiles etc ( Currently only server port is controlled; default 8080 - Update as required)


Running the server
------------------

To build and run the server from source.   
./build.sh  
./start.sh

Distribution folder dist/ will be created. This folder contains standalone jar and logs folder.

Stopping the server
-------------------
./stop.sh



PS: If you are on windows, you can alternatively build directly using maven and run the standalone jar under target/ using command - "java -jar number26-txns-0.1.0.jar"



