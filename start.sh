#!/bin/bash
 
# exit on any error
set -o errexit

echo "Starting server.."
echo "Logging at dist/logs"

nohup java -jar -Dspring.config.location=config/application.yml -Dlogging.config=config/logback.xml dist/number26-txns-0.1.0.jar > /dev/null 2>&1 &

