#!/bin/bash
 
# exit on any error
set -o errexit

if [ ! -d "dist" ]; then
  mkdir dist
  mkdir -p dist/logs
fi

mvn clean install

rm -f dist/*.jar

cp target/*.jar dist
